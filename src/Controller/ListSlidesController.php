<?php

namespace App\Controller;

use App\Entity\Slide;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ListSlidesController extends AbstractController
{
    /**
     * @Route("/list/slides", name="list_slides")
     */
    public function index(EntityManagerInterface $em)
    {
		$slides = $em->getRepository(Slide::class)->findAll();
        return $this->render('list_slides/index.html.twig', [
            'ListSlides'=>$slides,
        ]);
    }
}

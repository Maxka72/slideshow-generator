<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Slide;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SlideViewerController extends AbstractController
{
    /**
     * @Route("/slide/viewer", name="slide_viewer")
     */
	/**
     * @Route("/slide/viewer/{id}", name="slide_viewer", requirements={"id"="\d+"})
     */
    public function index(EntityManagerInterface $em, int $id)
    {
        $slide = $em->getRepository(Slide::class)->find($id);
		if (null === $slide) {
			throw new NotFoundHttpException();
		}
		
		return $this->render('slide_viewer/index.html.twig', [
            'slide' => $slide,
        ]);
    }
}

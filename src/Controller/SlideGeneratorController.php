<?php

namespace App\Controller;

use App\Form\Type\SlideType;
use App\Form\Type\ImageType;
use App\Entity\Slide;
use App\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class SlideGeneratorController extends AbstractController
{
	/**
     * @Route("/slide/generator/index", name="slide_generator_index")
     */
    public function index(EntityManagerInterface $em)
    {
        return $this->render('slide_generator/index.html.twig', [
        ]);
    }
	
    /**
     * @Route("/slide/generator/new", name="slide_generator_new")
     */
    public function new(EntityManagerInterface $em, Request $request)
    {
		$slide = new Slide();

        $form = $this->createForm(SlideType::class, $slide);
		
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			// On récupère les données du formulaire
			$slide = $form->getData();

			// On ajoute une entité Slide dans la BDD
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($slide);
			$entityManager->flush();

			// On redirige vers la page pour configuer le diaporama une fois le titre valider
			return $this->redirectToRoute('slide_generator_configurator', ['id' => strval($slide->getId())]);
		}
		
		return $this->render('slide_generator/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
	
	/**
     * @Route("/slide/generator/configurator/{id}", name="slide_generator_configurator", requirements={"id"="\d+"})
     */
    public function configurator(EntityManagerInterface $em, Request $request, int $id, SluggerInterface $slugger)
    {		
		$slide = $em->getRepository(Slide::class)->find($id);
		if (null === $slide) {
			throw new NotFoundHttpException();
		}
		
		$image = new Image();

        $form = $this->createForm(ImageType::class, $image);
		
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			// On récupère les données du formulaire
			$image = $form->getData();
			
			/** Image upload */
            $imageUrl = $form->get('url')->getData();

            if ($imageUrl) {
                $originalFilename = pathinfo($imageUrl->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$imageUrl->guessExtension();

                // On sauvegarde l'image dans le dossier
                try {
                    $imageUrl->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // En cas d'erreur
                }

                // On sauvegarde le nom du fichier
                $image->setUrl($newFilename);
			}
			
			$image->setSlide($slide);

			// On ajoute une entité Image dans la BDD
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($image);
			$entityManager->flush();

			// On redirige vers la page pour configuer le diaporama une fois le titre valider
			return $this->redirectToRoute('slide_generator_configurator', ['id' => strval($image->getSlide()->getId())]);
		}
	
		return $this->render('slide_generator/configurator.html.twig', [
            'form' => $form->createView(),
			'slide' => $slide,
        ]);
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Slide", inversedBy="images")
     * @ORM\JoinColumn(nullable=false)
     */
    private $slide;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="integer")
     */
    private $durationEffect;

    /**
     * @ORM\Column(type="integer")
     */
    private $xStart;

    /**
     * @ORM\Column(type="integer")
     */
    private $yStart;
	
	/**
     * @ORM\Column(type="integer")
     */
    private $widthStart;
	
	/**
     * @ORM\Column(type="integer")
     */
    private $heightStart;

    /**
     * @ORM\Column(type="integer")
     */
    private $xEnd;

    /**
     * @ORM\Column(type="integer")
     */
    private $yEnd;
	
	/**
     * @ORM\Column(type="integer")
     */
    private $widthEnd;
	
	/**
     * @ORM\Column(type="integer")
     */
    private $heightEnd;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlide(): ?Slide
    {
        return $this->slide;
    }

    public function setSlide(?Slide $slide): self
    {
        $this->slide = $slide;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getDurationEffect(): ?int
    {
        return $this->durationEffect;
    }

    public function setDurationEffect(int $durationEffect): self
    {
        $this->durationEffect = $durationEffect;

        return $this;
    }

    public function getXStart(): ?int
    {
        return $this->xStart;
    }

    public function setXStart(int $xStart): self
    {
        $this->xStart = $xStart;

        return $this;
    }

    public function getYStart(): ?int
    {
        return $this->yStart;
    }

    public function setYStart(int $yStart): self
    {
        $this->yStart = $yStart;

        return $this;
    }
	
	public function getWidthStart(): ?int
    {
        return $this->widthStart;
    }

    public function setWidthStart(int $widthStart): self
    {
        $this->widthStart = $widthStart;

        return $this;
    }
	
	public function getHeightStart(): ?int
    {
        return $this->heightStart;
    }

    public function setHeightStart(int $heightStart): self
    {
        $this->heightStart = $heightStart;

        return $this;
    }

    public function getXEnd(): ?int
    {
        return $this->xEnd;
    }

    public function setXEnd(int $xEnd): self
    {
        $this->xEnd = $xEnd;

        return $this;
    }

    public function getYEnd(): ?int
    {
        return $this->yEnd;
    }

    public function setYEnd(int $yEnd): self
    {
        $this->yEnd = $yEnd;

        return $this;
    }
	
	public function getWidthEnd(): ?int
    {
        return $this->widthEnd;
    }

    public function setWidthEnd(int $widthEnd): self
    {
        $this->widthEnd = $widthEnd;

        return $this;
    }
	
	public function getHeightEnd(): ?int
    {
        return $this->heightEnd;
    }

    public function setHeightEnd(int $heightEnd): self
    {
        $this->heightEnd = $heightEnd;

        return $this;
    }
}

<?php

namespace App\Form\Type;

use App\Entity\Image;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('slide', HiddenType::class)
			->add('title', TextType::class, ['attr' => ['maxlength' => 20]])
			->add('url', FileType::class, [
                'label' => 'Image',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image (png or jpeg)',
                    ])
                ],
            ])
			->add('durationEffect', IntegerType::class, ['attr' => ['value' => 5]])
			->add('xStart', HiddenType::class, ['attr' => ['value' => 200]])
			->add('yStart', HiddenType::class, ['attr' => ['value' => 200]])
			->add('widthStart', HiddenType::class, ['attr' => ['value' => 100]])
			->add('heightStart', HiddenType::class, ['attr' => ['value' => 100]])
			->add('xEnd', HiddenType::class, ['attr' => ['value' => 300]])
			->add('yEnd', HiddenType::class, ['attr' => ['value' => 300]])
			->add('widthEnd', HiddenType::class, ['attr' => ['value' => 100]])
            ->add('heightEnd', HiddenType::class, ['attr' => ['value' => 100]])
			->add('save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary btn-xl js-scroll-trigger']])
        ;
    }
	
	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Image::class,
        ]);
    }
}
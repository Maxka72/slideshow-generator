<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200422140445 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE image ADD title VARCHAR(20) DEFAULT NULL, ADD url VARCHAR(255) NOT NULL, ADD duraction_effect INT NOT NULL, ADD x_astart INT NOT NULL, ADD y_astart INT NOT NULL, ADD x_bstart INT NOT NULL, ADD y_bstart INT NOT NULL, ADD x_aend INT NOT NULL, ADD y_aend INT NOT NULL, ADD x_bend INT NOT NULL, ADD y_bend INT NOT NULL');
        $this->addSql('ALTER TABLE slide CHANGE title title VARCHAR(50) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE image DROP title, DROP url, DROP duraction_effect, DROP x_astart, DROP y_astart, DROP x_bstart, DROP y_bstart, DROP x_aend, DROP y_aend, DROP x_bend, DROP y_bend');
        $this->addSql('ALTER TABLE slide CHANGE title title VARCHAR(50) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}

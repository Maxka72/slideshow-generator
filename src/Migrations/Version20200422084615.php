<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200422084615 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE slide_image');
        $this->addSql('ALTER TABLE image DROP INDEX UNIQ_C53D045FDD5AFB87, ADD INDEX IDX_C53D045FDD5AFB87 (slide_id)');
        $this->addSql('ALTER TABLE slide ADD title VARCHAR(50) DEFAULT NULL, DROP titre');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE slide_image (slide_id INT NOT NULL, image_id INT NOT NULL, INDEX IDX_44B4D27C3DA5256D (image_id), INDEX IDX_44B4D27CDD5AFB87 (slide_id), PRIMARY KEY(slide_id, image_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE slide_image ADD CONSTRAINT FK_44B4D27C3DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE slide_image ADD CONSTRAINT FK_44B4D27CDD5AFB87 FOREIGN KEY (slide_id) REFERENCES slide (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE image DROP INDEX IDX_C53D045FDD5AFB87, ADD UNIQUE INDEX UNIQ_C53D045FDD5AFB87 (slide_id)');
        $this->addSql('ALTER TABLE slide ADD titre VARCHAR(50) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, DROP title');
    }
}

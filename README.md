# Génerateur de Slider
Génerateur de Slider

## Config :


## Done

### Page d'accueil : 
+ Bouton pour accéder à la création d'un diaporama
+ Barre de navigation : rafraichir la page, création, visualisation

### Page de création :
+ Ajout du nom du diaporama

### Page de configuration :
+ Prévisualisation de l'image importée
+ Choix du titre de l'image
+ Choix de la durée d'événement
+ Choix du point focal de départ et du point focal d’arrivée

### Page de visualisation :

+ liste des diaporamas disponible
+ lecture des diaporamas disponible

### Barre de navigation :
+ Slideshow Generator : page d'accueil
+ Créer : page de crétion de diaporama
+ Visualiser : page de visualisation de diaporama


## RAF :


#### VILLERMIN Maxime - GEMY Dorian